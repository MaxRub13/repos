#pragma once
#include "resource.h"
#include <shellapi.h>
#include <commctrl.h>
#include <filesystem>
#define IDC_TREE 21

class myResults
{
public:
	myResults();
	void createResults(HINSTANCE, HWND);
	myResults(const myResults&) = delete;
	myResults& operator=(const myResults&) = delete;
	~myResults();
	HWND& getHWND() { return myRWnd; };
private:
	static LRESULT CALLBACK ResultsProc(HWND, UINT, WPARAM, LPARAM);
	HINSTANCE myRInst;
	HWND myRWnd;
};