#include "framework.h"
#include "myResults.h"
HINSTANCE hInstR;                                // current instance

myResults::myResults()
{
    //blank constructor
};
void myResults::createResults(HINSTANCE hInstance, HWND hWndParent)
{
    const wchar_t* NAME_CLASS = L"Search results";

    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = ResultsProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYFIND));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYFIND);
    wcex.lpszClassName = NAME_CLASS;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    if(!RegisterClassExW(&wcex))
    {
        MessageBox(hWndParent, L"Call to RegisterClassEx failed!!", L"Alert!", MB_OK | MB_ICONWARNING);
    }

    hInstR = hInstance; // Store instance handle in our global variable

    myRWnd = CreateWindowW(NAME_CLASS, L"Search results:", WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_TABSTOP | WS_DLGFRAME,
        CW_USEDEFAULT, 0, 650, 500, hWndParent, nullptr, hInstance, nullptr);

    if (!myRWnd)
    {
        MessageBox(hWndParent, L"Failed create window!", L"Alert!", MB_OK | MB_ICONWARNING);
    }

    ShowWindow(myRWnd, SW_SHOW);
    UpdateWindow(myRWnd);

}
myResults::~myResults()
{
    const wchar_t* NAME_CLASS = L"Search results";
    UnregisterClass(NAME_CLASS, myRInst);
}
LRESULT CALLBACK myResults::ResultsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    InitCommonControls;
    TV_ITEM tv_item;
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_CREATE:
        CreateWindowW(WC_TREEVIEW, L"",
            WS_VISIBLE | WS_CHILD | WS_BORDER |
            TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT,
            0, 20, 600, 280,
            hDlg, (HMENU)IDC_TREE, hInstR, NULL);
        return (INT_PTR)TRUE;
    case WM_NOTIFY:
    {
        switch (reinterpret_cast<LPNMHDR>(lParam)->code)
        {
        case NM_DBLCLK:
            TCHAR Text[255] = L"";                  //retrieve text from item to here
            memset(&tv_item, 0, sizeof(tv_item));   //set all items to 0
            HTREEITEM Selected = (HTREEITEM)SendDlgItemMessage(hDlg, IDC_TREE,
                TVM_GETNEXTITEM, TVGN_CARET, (LPARAM)&tv_item);
            if (Selected == NULL)
            {
                MessageBox(hDlg, L"No Items in TreeView",
                    L"Error", MB_OK | MB_ICONINFORMATION);
                break;
            }
            SendDlgItemMessage(hDlg, IDC_TREE, TVM_ENSUREVISIBLE, 0, (LPARAM)Selected);
            SendDlgItemMessage(hDlg, IDC_TREE, TVM_SELECTITEM, TVGN_CARET, (LPARAM)Selected);
            tv_item.mask = TVIF_TEXT;
            tv_item.pszText = Text;
            tv_item.cchTextMax = 256;
            tv_item.hItem = Selected;
            SendDlgItemMessage(hDlg, IDC_TREE, TVM_GETITEM, 0, (LPARAM)&tv_item);
            if (!std::filesystem::exists(Text))                      //check path to file, C++17
            {
                MessageBox(hDlg, L"Please double click on file name!", L"Alert!", MB_OK | MB_ICONWARNING);
            }
            else
            {
                HINSTANCE i = ShellExecuteW(NULL, NULL, Text, NULL, NULL, SW_SHOWNORMAL);
                if (i < (HINSTANCE)32)
                    MessageBox(hDlg, L"Function fails!!!", L"Alert!", MB_OK | MB_ICONWARNING);
            }
            break;
        }
        break;
    }
    case WM_COMMAND:
        int wmId = LOWORD(wParam);
        switch (wmId)
        {
        case IDOK:
        case IDCANCEL:
            //hResultsDialog = NULL;             //for open new results window if it has been closed
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
            //       break;
        }
    }
    return (INT_PTR)FALSE;
}