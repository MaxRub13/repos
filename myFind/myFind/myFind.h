#pragma once

#include "resource.h"
#include "myResults.h"
#include <shlobj.h>
#include <commctrl.h>
#include <commdlg.h>
#include <winbase.h>
#include <atomic>
#include <fstream>
#include <vector>
#include <string>
#include <filesystem>
#include <atomic>


#define CB_FIND 11
#define CB_FILTERS 12
#define CB_DIR 13
#define B_FIND_ALL 14
#define B_CLOSE 15
#define B_DIR 16
#define CHB_DOC 17
#define CHB_FOL 18
#define CHB_HID 19
#define ID_CANCEL_SEARCH 20
//HWND hProgress;
//DWORD ThreadId;
//HANDLE hFindThread;
//std::atomic<BOOL> stopped{ false };//using for cancel search
DWORD MyFindFunction(HWND hFindDialog);
int myFindFunctionStr(const std::wstring& line, const std::wstring& find_str, int pos);

class myFind
{
public:
	myFind(HINSTANCE);
	myFind(const myFind&) = delete;
	myFind& operator=(const myFind&) = delete;
	~myFind();

private:
	void myFindCreate();
	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	HINSTANCE myInst;
	HWND myWnd;
};