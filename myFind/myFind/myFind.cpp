// myFind.cpp : Defines the entry point for the application.
//
#include "framework.h"
#include "myFind.h"
#define MAX_LOADSTRING 100
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HINSTANCE hInst;                                // current instance
HWND hCfind, hCfilters, hCdir;
std::atomic<BOOL> stopped{ false };//using for cancel search
HWND hProgress;


myFind::myFind(HINSTANCE hInstance)
{
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYFIND, szWindowClass, MAX_LOADSTRING); 

    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYFIND));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYFIND);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    RegisterClassExW(&wcex);

   hInst = hInstance; // Store instance handle in our global variable

   myWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_TABSTOP | WS_DLGFRAME ,
      CW_USEDEFAULT, 0, 650, 300, nullptr, nullptr, hInstance, nullptr);

   if (!myWnd)
   {
      //return FALSE;
   }

   ShowWindow(myWnd, SW_SHOW);
   UpdateWindow(myWnd);

}
myFind::~myFind()
{
    UnregisterClass(szWindowClass, myInst);
}

LRESULT CALLBACK myFind::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    //InitCommonControls;
    DWORD ThreadId;
    HANDLE hFindThread;
    static TCHAR name[256] = _T("");
    static OPENFILENAME file;
    static BROWSEINFO dir;
    myResults Obj;
    switch (message)
    {
    case WM_CREATE:
        //for to choice file
        file.lStructSize = sizeof(OPENFILENAME);
        file.hInstance = hInst;
        file.lpstrFilter = _T("All\0*.*\0Text\0*.txt\0");
        file.lpstrFile = name;
        file.nMaxFile = 256;
        file.lpstrInitialDir = _T(".\\");
        file.lpstrDefExt = _T("txt");
        //for to choice folder
        dir.hwndOwner = hWnd;
        dir.pidlRoot = NULL;
        dir.pszDisplayName = name;
        dir.lpszTitle = _T("Please, make your choice!");
        dir.lParam = NULL;
        dir.iImage = NULL;
        //create label
        CreateWindowW(L"static", L"Find what :", WS_VISIBLE | WS_CHILD, 40, 20, 70, 16, hWnd, NULL, hInst, NULL);
        CreateWindowW(L"static", L"Filters :", WS_VISIBLE | WS_CHILD, 60, 50, 50, 16, hWnd, NULL, hInst, NULL);
        CreateWindowW(L"static", L"Directory :", WS_VISIBLE | WS_CHILD, 20, 90, 70, 16, hWnd, NULL, hInst, NULL);
        //create combobox
        hCfind = CreateWindowW(L"combobox", L"", WS_VISIBLE | WS_CHILD | CBS_DROPDOWN | CBS_AUTOHSCROLL | WS_VSCROLL,
            112, 20, 350, 20, hWnd, (HMENU)CB_FIND, hInst, NULL);
        hCfilters = CreateWindowW(L"combobox", L"", WS_VISIBLE | WS_CHILD | CBS_DROPDOWN | CBS_AUTOHSCROLL | WS_VSCROLL,
            112, 50, 350, 20, hWnd, (HMENU)CB_FILTERS, hInst, NULL);
        hCdir = CreateWindowW(L"combobox", L"", WS_VISIBLE | WS_CHILD | CBS_DROPDOWN | CBS_AUTOHSCROLL | WS_VSCROLL,
            90, 90, 350, 20, hWnd, (HMENU)CB_DIR, hInst, NULL);
        //create button
        CreateWindowW(L"button", L"Find All ", WS_VISIBLE | WS_CHILD, 470, 20, 110, 25, hWnd, (HMENU)B_FIND_ALL, hInst, NULL);
        CreateWindowW(L"button", L"Close", WS_VISIBLE | WS_CHILD, 470, 50, 110, 25, hWnd, (HMENU)B_CLOSE, hInst, NULL);
        CreateWindowW(L"button", L"...", WS_VISIBLE | WS_CHILD, 450, 90, 25, 25, hWnd, (HMENU)B_DIR, hInst, NULL);
        CreateWindowW(L"button", L"Cancel", WS_VISIBLE | WS_CHILD, 250, 190, 110, 25, hWnd, (HMENU)ID_CANCEL_SEARCH, hInst, NULL);
        //create checkbox
        CreateWindowW(L"button", L"In current document.", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX , 480, 90, 100, 20, hWnd, (HMENU)CHB_DOC, hInst, NULL);
        CreateWindowW(L"button", L"In sub-folders", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX , 480, 110, 100, 20, hWnd, (HMENU)CHB_FOL, hInst, NULL);
        CreateWindowW(L"button", L"In hidden", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX , 480, 130, 100, 20, hWnd, (HMENU)CHB_HID, hInst, NULL);
        //create progress bar
        InitCommonControls;
        hProgress = CreateWindowW(PROGRESS_CLASS, (LPTSTR)NULL, WS_CHILD | WS_VISIBLE, 20, 170, 600, 15, hWnd, (HMENU)0, hInst, NULL);

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case ID_CANCEL_SEARCH:
                //for cancel search
                stopped.store(true, std::memory_order_relaxed);
                break;
            case B_FIND_ALL:
                //Obj.createResults(hInst, hWnd);//failed create windows
                SendMessage(hProgress,  PBM_SETRANGE, 0, 1);
                //set FALSE for new search
                stopped.store(false, std::memory_order_relaxed);
                //crated new thread for running MyFindFunction
                hFindThread = CreateThread(NULL, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(MyFindFunction),
                    hWnd, NULL, &ThreadId);
                if (hFindThread != NULL)
                {
                    WaitForSingleObject(hFindThread, 1);
                    CloseHandle(hFindThread);
                }
                break;
            case B_DIR:
                //open browse to choose file for search 
                if (IsDlgButtonChecked(hWnd, CHB_DOC))
                {
                    file.Flags = OFN_HIDEREADONLY;
                    if (!GetOpenFileName(&file))
                        return 1;
                    SetDlgItemTextW(hWnd, CB_DIR, name);
                    InvalidateRect(hWnd, NULL, TRUE);
                    break;
                }
                else if (IsDlgButtonChecked(hWnd, CHB_FOL))
                    //open browse to choose folder for search 
                {
                    dir.ulFlags = BIF_DONTGOBELOWDOMAIN;
                    {
                        ITEMIDLIST* piidl;
                        piidl = SHBrowseForFolder(&dir);
                        if (piidl != NULL)
                            SHGetPathFromIDList(piidl, name);
                    }
                    SetDlgItemTextW(hWnd, CB_DIR, name);
                    InvalidateRect(hWnd, NULL, TRUE);
                    break;
                }
                else if (IsDlgButtonChecked(hWnd, CHB_HID))
                {
                    MessageBox(hWnd, L"In hidden folders!", L"Alert!", MB_OK | MB_ICONWARNING);
                }
                else
                    MessageBox(hWnd, L"Please, choose where you want search!", L"Alert!", MB_OK | MB_ICONWARNING);
                break;
            /*case IDCANCEL:
                EndDialog(hProgress, LOWORD(wParam));
                break;*/
            case B_CLOSE:
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
DWORD MyFindFunction(HWND hDlg)
{
    using namespace std::filesystem;				//pay attention C++17 Standart
    std::wifstream wfin;                             //for std::wstring we need use std::wifstream
    std::wofstream wfout;                            //for std::wstring we need use std::wofstream
    TCHAR data[256] = _T("");
    std::vector<std::wstring> pvFilters_str;		//vector for store filters
    std::vector<std::wstring> pvPath_str;        //for store path to files
    double countForMaximumProgressBar = 0;
    double currentSize = 0;

    //for Tree View
    HTREEITEM hPrev;
    HTREEITEM hRoot;
    static TV_INSERTSTRUCT TV_insertstruct;
    static TV_ITEM TV_item;

    //geting data for research 
    std::string tempFind, tempFilters, tempDir;
    GetDlgItemText(hDlg, CB_FIND, data, 256);
    for (int i = 0; i < 256; i++)
        tempFind += (char*)data + i;
    std::wstring strFindWhat(tempFind.begin(), tempFind.end()); //get data Find What
    //create Search results string
    std::wstring SearchLine = L" Search in file(s) \" " + strFindWhat + L" \":";;
    GetDlgItemText(hDlg, CB_FILTERS, data, 256);
    for (int i = 0; i < 256; i++)
        tempFilters += (char*)data + i;
    std::wstring strFilters(tempFilters.begin(), tempFilters.end()); //get data Filters
    GetDlgItemText(hDlg, CB_DIR, data, 256);
    for (int i = 0; i < 256; i++)
        tempDir += (char*)data + i;
    std::wstring strDirectory(tempDir.begin(), tempDir.end()); //get data Directory
    // 
    // 
    // 
    //create Search results string
    //starting searching
    TV_item.mask = TVIF_TEXT;
    TV_item.pszText = (LPWSTR)SearchLine.c_str();
    TV_item.cChildren = NULL;

    TV_insertstruct.hInsertAfter = TVI_LAST;
    TV_insertstruct.hParent = TVI_ROOT;
    TV_insertstruct.item = TV_item;
    //set root item for tree view
    //LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
    //hRoot = (HTREEITEM)Temp;
    //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x000000FF);//sets the text color to red
    ShowWindow(hProgress, SW_SHOW);
    //
    // 
    // 
    // 
    //for current file only.............................................................
    if (IsDlgButtonChecked(hDlg, CHB_DOC))
    {
        int countToEmpty = 0;               //for cheking file
        TV_item.pszText = (LPWSTR)strDirectory.c_str();
        TV_item.cChildren = 1;
        //TV_insertstruct.hParent = hRoot;
        //TV_insertstruct.item = TV_item;
        //SendDlgItemMessage(hResults, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x0000FF00);//sets the text color to green 
        //LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
        //InvalidateRect(hResultsDialog, NULL, TRUE);
        //hPrev = (HTREEITEM)Temp;
        if (!exists(strDirectory))                      //check file
        {
            MessageBox(hDlg, L"No existing file", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {

            std::wstring line;
            int count_line = 0;
            wfin.open(strDirectory);

            if (wfin.is_open())
            {
                while (getline(wfin, line) && !stopped.load(std::memory_order_relaxed))
                {
                    count_line++;
                    int count = myFindFunctionStr(line, strFindWhat, 0);
                    if (count > 0)
                    {
                        countToEmpty++;
                        std::wstring count_wstr = std::to_wstring(count);
                        std::wstring num = std::to_wstring(count_line);
                        std::wstring lineResult = L"Line " + num + L" have " + count_wstr + L" hit(s): " + line;
                        TV_item.pszText = (LPWSTR)lineResult.c_str();
                        TV_item.cChildren = 1;
                        //TV_insertstruct.hParent = hPrev;
                        //TV_insertstruct.item = TV_item;
                        //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                        //InvalidateRect(hResultsDialog, NULL, TRUE);
                        count = 0;
                    }
                }
                SendMessage(hProgress, PBM_SETSTEP, (WPARAM)1, 0);
                count_line = 0;
                SendMessage(hProgress, PBM_STEPIT, 0, 0);
                InvalidateRect(hProgress, NULL, TRUE);
            }
            else if (!stopped.load(std::memory_order_relaxed))
                MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
            if (stopped.load(std::memory_order_relaxed))
            {
                MessageBox(hDlg, L"Search in current document was canceled!!!", L"MessageBox", MB_OK | MB_ICONWARNING);
            }
            wfin.close();
            //InvalidateRect(hProgressDialog, NULL, TRUE);
        }
        //if (countToEmpty == 0)
            //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_DELETEITEM, 0, (LPARAM)hPrev);
    }
    //.........................................................................	
    else if (IsDlgButtonChecked(hDlg, CHB_FOL))
    {
        if (!exists(strDirectory))                      //check directory
        {
            MessageBox(hDlg, L"No existing directory", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            //filters string processing.................................................
            std::wstring temp;
            for (int i = 0; i < strFilters.size() || strFilters[i] != '\0'; i++)			//pay attention need '*' in filters			
            {
                if (strFilters[i] == '*')
                    continue;
                else if (strFilters[i] == ';' || strFilters[i] == ' ')
                {
                    if (temp.size() > 0)
                    {
                        pvFilters_str.push_back(temp);
                        temp.clear();
                    }
                    else
                        continue;
                }
                else
                    temp += strFilters[i];
            }
            pvFilters_str.push_back(temp);
            temp.clear();
        }
        for (const auto& entry : recursive_directory_iterator(strDirectory))
        {
            std::filesystem::path temp_fs_path = entry.path();
            for (int i = 0; i < pvFilters_str.size(); i++)
            {
                if (pvFilters_str[i] == temp_fs_path.extension())        //check files extension and save path in vector
                {
                    //here we need find size of files for progress Bar
                    countForMaximumProgressBar += std::filesystem::file_size(temp_fs_path);
                    std::wstring data = temp_fs_path.wstring();
                    pvPath_str.push_back(data);
                }
            }

        }
        if (pvPath_str.size() == 0)
        {
            MessageBox(hDlg, L"No files with this filters!", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            std::wstring data;
            std::wstring line;
            int count_line = 0;
            //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x0000FF00);//sets the text color to green 
            for (int i = 0; i < pvPath_str.size(); i++)
            {
                int countToEmpty = 0;               //for cheking file
                data = pvPath_str[i];				//pay attention 
                wfin.open(pvPath_str[i]);
                TV_item.pszText = (LPWSTR)pvPath_str[i].c_str();
                TV_item.cChildren = 1;
                //TV_insertstruct.hParent = hRoot;
                TV_insertstruct.item = TV_item;
                //LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                //InvalidateRect(hResultsDialog, NULL, TRUE);
                //hPrev = (HTREEITEM)Temp;
                if (wfin.is_open() && !stopped.load(std::memory_order_relaxed))
                {
                    while (std::getline(wfin, line))
                    {
                        count_line++;
                        int count = myFindFunctionStr(line, strFindWhat, 0);
                        if (count > 0)
                        {
                            countToEmpty++;
                            std::wstring count_wstr = std::to_wstring(count);
                            std::wstring num = std::to_wstring(count_line);
                            std::wstring lineResult = L"Line " + num + L" have " + count_wstr + L" hit(s): " + line;
                            TV_item.pszText = (LPWSTR)lineResult.c_str();
                            TV_item.cChildren = 1;
                            //TV_insertstruct.hParent = hPrev;
                            TV_insertstruct.item = TV_item;
                            //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                            //InvalidateRect(hResultsDialog, NULL, TRUE);
                            count = 0;
                        }
                    }
                    count_line = 0;
                    currentSize += (double)file_size(data);
                    double sizeStep = currentSize / countForMaximumProgressBar;
                    SendMessage(hProgress, PBM_SETSTEP, (WPARAM)sizeStep, 0);
                    SendMessage(hProgress, PBM_STEPIT, 0, 0);
                    InvalidateRect(hProgress, NULL, TRUE);

                }
                else if (!stopped.load(std::memory_order_relaxed))
                    MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
                else
                {
                    MessageBox(hDlg, L"Search in folders was canceled!!!", L"MessageBox", MB_OK | MB_ICONWARNING);
                    break;
                }
                wfin.close();
                //if we do not find a match, we delete this file from the tree view
                //if (countToEmpty == 0)
                    //SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_DELETEITEM, 0, (LPARAM)hPrev);
            }
        }
    }
    else if (IsDlgButtonChecked(hDlg, CHB_HID))
    {
        MessageBox(hDlg, L"In hidden folders", L"Done!", MB_OK | MB_ICONWARNING);
    }
    return 1;
}
int myFindFunctionStr(const std::wstring& line, const std::wstring& find_str, int pos)
{
    int count = 0;
    int count_p = line.find(find_str, pos);
    if (count_p != std::wstring::npos)
    {
        if (count_p + find_str.size() <= line.size())
        {
            count++;
            return count + myFindFunctionStr(line, find_str, count_p + (find_str.size() - 1));
        }
        else
            return 0;
    }
    else
        return 0;
}