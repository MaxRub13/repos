#pragma once

#include "resource.h"
#include <winbase.h>
#include <process.h>
#include <commdlg.h>
#include <commctrl.h>
#include <shellapi.h>
#include <shlobj.h>
#include <atomic>
#include <fstream>
#include <vector>
#include <string>
#include <filesystem>


#define MAX_LOADSTRING 100
std::atomic<BOOL> stopped{ false };//using for cancel search
HWND hFindDialog, hResultsDialog, hProgressDialog, hTreeResults;
DWORD ThreadId;
HANDLE hFindThread;
HTREEITEM hRoot, hLine, hSubLine;
DWORD MyFindFunction(HWND hFindDialog);
int myFindFunctionStr(const std::wstring& line, const std::wstring& find_str, int pos);


