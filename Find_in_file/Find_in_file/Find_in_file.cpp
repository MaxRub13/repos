// Find_in_file.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "Find_in_file.h"

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
//INT_PTR CALLBACK    ProgressProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    FindProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    ResultProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FINDINFILE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FINDINFILE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            if (!IsDialogMessage(hFindDialog, &msg))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
    }

    return (int) msg.wParam;
}
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW ;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FINDINFILE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FINDINFILE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 750, 500, nullptr, nullptr, hInstance, nullptr);
   //HWND hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGBAR), NULL, (DLGPROC)WndProc);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;
    static TCHAR name[256] = _T("");
    static OPENFILENAME file;
    std::wifstream in;
    std::wofstream out;
    static std::vector<std::wstring> v;
    std::vector<std::wstring>::iterator it;
    std::wstring st;
    int y, k;
    static int n, length, sx, sy, cx, iVscrollPos, iHscrollPos, COUNT, MAX_WIDTH;
    static SIZE size = { 8, 16 };

    switch (message)
    {
    case WM_CREATE:
        //create buttons
        file.lStructSize = sizeof(OPENFILENAME);
        file.hInstance = hInst;
        file.lpstrFilter = _T("All\0*.*\0Text\0*.txt\0");
        file.lpstrFile = name;
        file.nMaxFile = 256;
        file.lpstrInitialDir = _T(".\\");
        file.lpstrDefExt = _T("txt");
        return 0;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case ID_FIND_FINDINFILES:
                hFindDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGBAR), hWnd, FindProc);
                ShowWindow(hFindDialog, SW_SHOW);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

INT_PTR CALLBACK FindProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    static TCHAR name[256] = _T("");
    static OPENFILENAME file;
    static BROWSEINFO dir;
    std::wifstream in;

    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
        InitCommonControls;
    case WM_INITDIALOG:
        //for to choice file
        file.lStructSize = sizeof(OPENFILENAME);
        file.hInstance = hInst;
        file.lpstrFilter = _T("All\0*.*\0Text\0*.txt\0");
        file.lpstrFile = name;
        file.nMaxFile = 256;
        file.lpstrInitialDir = _T(".\\");
        file.lpstrDefExt = _T("txt");
        //for to choice folder
        dir.hwndOwner = hDlg;
        dir.pidlRoot = NULL;
        dir.pszDisplayName = name;
        dir.lpszTitle = _T("Please, make your choice!");
        dir.lParam = NULL;
        dir.iImage = NULL;
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        int wmId = LOWORD(wParam);
        switch (wmId)
        {
        case ID_CANCEL_SEARCH:
            //for cancel search
            stopped.store(true, std::memory_order_relaxed);
            break;
        case IDCANCEL:           
        case IDC_BUTTON_CLOSE:
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
            break;
        case IDC_FINDALL:
            if(hResultsDialog == NULL)
                hResultsDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DLG_RESULTS), hDlg, ResultProc);
            InitCommonControls;
            hProgressDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DLG_PROGRESS), hDlg, FindProc);
            SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETRANGE, 0, 1);
            //set FALSE for new search
            stopped.store(false, std::memory_order_relaxed); 
            //crated new thread for running MyFindFunction
            hFindThread = CreateThread(NULL, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(MyFindFunction),
                                       hDlg, NULL, &ThreadId);
            if(hFindThread != NULL)
            {
                WaitForSingleObject(hFindThread, 1);
                CloseHandle(hFindThread);
            }
            break;
        case IDC_BUTTON_DIRECTORY:
            //open browse to choose file for search 
            if(IsDlgButtonChecked(hDlg, IDC_CHECK1))
            {
                file.Flags = OFN_HIDEREADONLY;
                if (!GetOpenFileName(&file))
                    return 1;
                SetDlgItemTextW(hDlg, IDC_COMBO_DIR, name);
                InvalidateRect(hDlg, NULL, TRUE);
                break;
            }
            else if (IsDlgButtonChecked(hDlg, IDC_CHECK2))
            //open browse to choose folder for search 
            {
                dir.ulFlags = BIF_DONTGOBELOWDOMAIN;
                {
                    ITEMIDLIST* piidl;
                    piidl = SHBrowseForFolder(&dir);
                    if (piidl != NULL)
                        SHGetPathFromIDList(piidl, name);
                }
                SetDlgItemTextW(hDlg, IDC_COMBO_DIR, name);
                InvalidateRect(hDlg, NULL, TRUE);
                break;
            }
            else if (IsDlgButtonChecked(hDlg, IDC_CHECK3))
            {
                MessageBox(hDlg, L"In hidden folders!", L"Alert!", MB_OK | MB_ICONWARNING);
            }
            else
                MessageBox(hDlg, L"Please, choose where you want search!", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        break;
    }
    return (INT_PTR)FALSE;
}
INT_PTR CALLBACK ResultProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    TV_ITEM tv_item;
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;
    case WM_NOTIFY:
    {
        switch (reinterpret_cast<LPNMHDR>(lParam)->code)
        {
        case NM_DBLCLK:
            TCHAR Text[255] = L"";                  //retrieve text from item to here
            memset(&tv_item, 0, sizeof(tv_item));   //set all items to 0
            HTREEITEM Selected = (HTREEITEM)SendDlgItemMessage(hDlg, IDC_TREE1,
                TVM_GETNEXTITEM, TVGN_CARET, (LPARAM)&tv_item);
            if (Selected == NULL)
            {
                MessageBox(hDlg, L"No Items in TreeView",
                    L"Error", MB_OK | MB_ICONINFORMATION);
                break;
            }
            SendDlgItemMessage(hDlg, IDC_TREE1, TVM_ENSUREVISIBLE,0, (LPARAM)Selected);
            SendDlgItemMessage(hDlg, IDC_TREE1, TVM_SELECTITEM, TVGN_CARET, (LPARAM)Selected);
            tv_item.mask = TVIF_TEXT;
            tv_item.pszText = Text;
            tv_item.cchTextMax = 256;
            tv_item.hItem = Selected;
            SendDlgItemMessage(hDlg, IDC_TREE1, TVM_GETITEM, 0, (LPARAM)&tv_item);
            if (!std::filesystem::exists(Text))                      //check path to file
            {
                MessageBox(hDlg, L"Please double click on file name!", L"Alert!", MB_OK | MB_ICONWARNING);
            }
            else
            {
                HINSTANCE i = ShellExecuteW(NULL, NULL, Text, NULL, NULL, SW_SHOWNORMAL);
                if (i < (HINSTANCE)32)
                    MessageBox(hDlg, L"Function fails!!!", L"Alert!", MB_OK | MB_ICONWARNING);
            }
            break;
        }
        break;
    }
    case WM_COMMAND:
        int wmId = LOWORD(wParam);
        switch (wmId)
        {
        case IDOK:
        case IDCANCEL:
            hResultsDialog = NULL;             //for open new results window if it has been closed
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
            //       break;
        }
    }
    return (INT_PTR)FALSE;
}
/*INT_PTR CALLBACK ProgressProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        int wmId = LOWORD(wParam);
        switch (wmId)
        {
        case ID_CANCEL_SEARCH:
            if (hFindThread != NULL)
            {
                DWORD exitCode = 0;
                MessageBox(hDlg, L"HELLO", L"MessageBox", MB_OK | MB_ICONWARNING);

                //ExitThread(exitCode);//test here, now exit from UI not from program
                TerminateThread(hFindThread, exitCode);
                if (CloseHandle(hFindThread))
                {
                     MessageBox(hDlg, L"Handle closed", L"MessageBox", MB_OK | MB_ICONWARNING);
                }
            }
            break;
            break;
        case IDCANCEL:
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        return (INT_PTR)FALSE;
        }
    }
}*/
DWORD MyFindFunction(HWND hDlg)
{
    using namespace std::filesystem;				//pay attention C++17 Standart
    std::wifstream wfin;                             //for std::wstring we need use std::wifstream
    std::wofstream wfout;                            //for std::wstring we need use std::wofstream
    TCHAR data[256] = _T("");
    std::vector<std::wstring> pvFilters_str;		//vector for store filters
    std::vector<std::wstring> pvPath_str;        //for store path to files
    double countForMaximumProgressBar = 0;
    double currentSize = 0;

    //for Tree View
    HTREEITEM hPrev;
    HTREEITEM hRoot;
    static TV_INSERTSTRUCT TV_insertstruct;
    static TV_ITEM TV_item;

    //geting data for research 
    std::string tempFind, tempFilters, tempDir;
    GetDlgItemText(hDlg, IDC_COMBO_FIND, data, 256);
    for (int i = 0; i < 256; i++)
        tempFind += (char*)data + i;
    std::wstring strFindWhat(tempFind.begin(), tempFind.end()); //get data Find What
    //create Search results string
    std::wstring SearchLine = L" Search in file(s) \" " + strFindWhat + L" \":";;
    GetDlgItemText(hDlg, IDC_COMBO_FILTERS, data, 256);
    for (int i = 0; i < 256; i++)
        tempFilters += (char*)data + i;
    std::wstring strFilters(tempFilters.begin(), tempFilters.end()); //get data Filters
    GetDlgItemText(hDlg, IDC_COMBO_DIR, data, 256);
    for (int i = 0; i < 256; i++)
        tempDir += (char*)data + i;
    std::wstring strDirectory(tempDir.begin(), tempDir.end()); //get data Directory
    // 
    // 
    // 
    //create Search results string
    //starting searching
    TV_item.mask = TVIF_TEXT;
    TV_item.pszText = (LPWSTR)SearchLine.c_str();
    TV_item.cChildren = NULL;

    TV_insertstruct.hInsertAfter = TVI_LAST;
    TV_insertstruct.hParent = TVI_ROOT;
    TV_insertstruct.item = TV_item;
    //set root item for tree view
    LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
    hRoot = (HTREEITEM)Temp;
    SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x000000FF);//sets the text color to red
    ShowWindow(hProgressDialog, SW_SHOW);
    //
    // 
    // 
    // 
    //for current file only.............................................................
    if (IsDlgButtonChecked(hDlg, IDC_CHECK1))
    {
        int countToEmpty = 0;               //for cheking file
        TV_item.pszText = (LPWSTR)strDirectory.c_str();
        TV_item.cChildren = 1;
        TV_insertstruct.hParent = hRoot;
        TV_insertstruct.item = TV_item;
        SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x0000FF00);//sets the text color to green 
        LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
        InvalidateRect(hResultsDialog, NULL, TRUE);
        hPrev = (HTREEITEM)Temp;
        if (!exists(strDirectory))                      //check file
        {
            MessageBox(hDlg, L"No existing file", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {

            std::wstring line;
            int count_line = 0;
            wfin.open(strDirectory);
            
            if (wfin.is_open())
            {
                while (getline(wfin, line) && !stopped.load(std::memory_order_relaxed))
                {
                    count_line++;
                    int count = myFindFunctionStr(line, strFindWhat, 0);
                    if (count > 0)
                    {
                        countToEmpty++;
                        std::wstring count_wstr = std::to_wstring(count);
                        std::wstring num = std::to_wstring(count_line);
                        std::wstring lineResult = L"Line " + num + L" have " + count_wstr + L" hit(s): " + line;
                        TV_item.pszText = (LPWSTR)lineResult.c_str();
                        TV_item.cChildren = 1;
                        TV_insertstruct.hParent = hPrev;
                        TV_insertstruct.item = TV_item;
                        SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                        InvalidateRect(hResultsDialog, NULL, TRUE);
                        count = 0;
                    }
                }
                SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETSTEP, (WPARAM)1, 0);
                count_line = 0;
                SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_STEPIT, 0, 0);
                InvalidateRect(hProgressDialog, NULL, TRUE);
            }
            else if(!stopped.load(std::memory_order_relaxed))
                MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
            if(stopped.load(std::memory_order_relaxed))
            {
                MessageBox(hDlg, L"Search in current document was canceled!!!", L"MessageBox", MB_OK | MB_ICONWARNING);
            }
            wfin.close();
            SendDlgItemMessage(hProgressDialog, IDCANCEL, BM_SETSTATE, TRUE, 0L);
            InvalidateRect(hProgressDialog, NULL, TRUE);
        }
        if(countToEmpty == 0)
            SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_DELETEITEM, 0, (LPARAM)hPrev);
    }
    //.........................................................................	
    else if (IsDlgButtonChecked(hDlg, IDC_CHECK2))
    {
        if (!exists(strDirectory))                      //check directory
        {
            MessageBox(hDlg, L"No existing directory", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            //filters string processing.................................................
            std::wstring temp;
            for (int i = 0; i < strFilters.size() || strFilters[i] != '\0'; i++)			//pay attention need '*' in filters			
            {
                if (strFilters[i] == '*')
                    continue;
                else if (strFilters[i] == ';' || strFilters[i] == ' ')
                {
                    if (temp.size() > 0)
                    {
                        pvFilters_str.push_back(temp);
                        temp.clear();
                    }
                    else
                        continue;
                }
                else
                    temp += strFilters[i];
            }
            pvFilters_str.push_back(temp);
            temp.clear();
        }
        for (const auto& entry : recursive_directory_iterator(strDirectory))
        {
            std::filesystem::path temp_fs_path = entry.path();
            for (int i = 0; i < pvFilters_str.size(); i++)
            {
                if (pvFilters_str[i] == temp_fs_path.extension())        //check files extension and save path in vector
                {
                    //here we need find size of files for progress Bar
                    countForMaximumProgressBar += std::filesystem::file_size(temp_fs_path);
                    std::wstring data = temp_fs_path.wstring();
                    pvPath_str.push_back(data);
                }
            }

        }
        if (pvPath_str.size() == 0)
        {
            MessageBox(hDlg, L"No files with this filters!", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else 
        {
            std::wstring data;
            std::wstring line;
            int count_line = 0;
            SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_SETTEXTCOLOR, 0, (LPARAM)0x0000FF00);//sets the text color to green 
            for (int i = 0; i < pvPath_str.size(); i++)
            {
                int countToEmpty = 0;               //for cheking file
                data = pvPath_str[i];				//pay attention 
                wfin.open(pvPath_str[i]);
                TV_item.pszText = (LPWSTR)pvPath_str[i].c_str();
                TV_item.cChildren = 1;
                TV_insertstruct.hParent = hRoot;
                TV_insertstruct.item = TV_item;
                LRESULT Temp = SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                InvalidateRect(hResultsDialog, NULL, TRUE);
                hPrev = (HTREEITEM)Temp;
                if (wfin.is_open() && !stopped.load(std::memory_order_relaxed))
                {
                    while (std::getline(wfin, line))
                    {
                        count_line++;
                        int count = myFindFunctionStr(line, strFindWhat, 0);
                        if (count > 0)
                        {
                            countToEmpty++;
                            std::wstring count_wstr = std::to_wstring(count);
                            std::wstring num = std::to_wstring(count_line);
                            std::wstring lineResult = L"Line " + num + L" have " + count_wstr + L" hit(s): " + line;
                            TV_item.pszText = (LPWSTR)lineResult.c_str();
                            TV_item.cChildren = 1;
                            TV_insertstruct.hParent = hPrev;
                            TV_insertstruct.item = TV_item;
                            SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_INSERTITEM, 0, (LPARAM)&TV_insertstruct);
                            InvalidateRect(hResultsDialog, NULL, TRUE);
                            count = 0;
                        }
                    }
                    count_line = 0;
                    currentSize += (double)file_size(data);
                    double sizeStep = currentSize / countForMaximumProgressBar;
                    SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETSTEP, sizeStep, 0);
                    SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_STEPIT, 0, 0);
                    InvalidateRect(hProgressDialog, NULL, TRUE);

                }
                else if (!stopped.load(std::memory_order_relaxed))
                    MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
                else
                {
                    MessageBox(hDlg, L"Search in folders was canceled!!!", L"MessageBox", MB_OK | MB_ICONWARNING);
                    break;
                }
                wfin.close();
                //if we do not find a match, we delete this file from the tree view
                if (countToEmpty == 0)
                    SendDlgItemMessage(hResultsDialog, IDC_TREE1, TVM_DELETEITEM, 0, (LPARAM)hPrev);
            }
            SendDlgItemMessage(hProgressDialog, IDCANCEL , BM_SETSTATE, TRUE, 0L);
            InvalidateRect(hProgressDialog, NULL, TRUE);
        }
    }
    else if (IsDlgButtonChecked(hDlg, IDC_CHECK3))
    {
        MessageBox(hDlg, L"In hidden folders", L"Done!", MB_OK | MB_ICONWARNING);
    }
    return 1;
}
int myFindFunctionStr(const std::wstring& line, const std::wstring& find_str, int pos)
{
    int count = 0;
    int count_p = line.find(find_str, pos);
    if (count_p != std::wstring::npos)
    {
        if (count_p + find_str.size() <= line.size())
        {
            count++;
            return count + myFindFunctionStr(line, find_str, count_p + (find_str.size() - 1));
        }
        else
            return 0;
    }
    else
        return 0;
}

/*void CharToString(std::string& str, char* data, int n)
{
    for (int i = 0; i < n; i++)
        str += data + i;
}*/